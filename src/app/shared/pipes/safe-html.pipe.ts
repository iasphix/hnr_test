import {Pipe, PipeTransform, Sanitizer, SecurityContext} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'safeHtml'
})
export class SafeHtmlPipe implements PipeTransform {

  constructor(private sanitizer: Sanitizer) {}

  transform(html): any {
    return this.sanitizer.sanitize(SecurityContext.HTML, html);
  }

}
