import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState, getLoaderStatus} from '../../store/index.reducer';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  showLoader$: Observable<boolean>

  constructor(private store: Store<AppState>) {
     this.showLoader$ = this.store.select(getLoaderStatus);
  }

  ngOnInit() {
  }

}
