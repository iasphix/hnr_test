import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [{
  path: '',
  loadChildren: './top/top.module#TopModule'
}, {
  path: 'item/:id',
  loadChildren: './items/items.module#ItemsModule'
}, {
  path: 'jobs',
  loadChildren: './jobs/jobs.module#JobsModule'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
