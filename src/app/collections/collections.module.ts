import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ItemPostComponent} from '../items/post/post.component';
import {VirtualScrollModule} from 'angular2-virtual-scroll';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    VirtualScrollModule
  ],
  declarations: [ItemPostComponent],
  exports: [
    ItemPostComponent,
    VirtualScrollModule
  ]
})
export class CollectionsModule { }
