import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import * as fromCollection from '../reducers/collections.reducers';
import * as fromReducer from '../index.reducer';
import * as ItemsCollections from '../actions/collection.actions';
import * as Item from '../actions/item.actions';
import {Router, CanActivate, ActivatedRouteSnapshot, ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ItemsService} from '../../items/items.service';
import {concatMap, filter, map, mergeMap, switchMap, take, toArray} from 'rxjs/operators';
import {ItemsCollectionModel} from '../../items/items.model';
import {ITEMS_PER_PAGE} from '../../app.config';


@Injectable()
export class ItemGuard implements CanActivate {
  constructor(private store: Store<fromCollection.State>,
              private itemsService: ItemsService) {
  }

  checkItemIsLoaded(itemId): Observable<boolean> {
    return this.store.select(fromReducer.getSingleItem(itemId)).pipe(
      map(item => {
        if (!item) {
          this.store.dispatch(new Item.GetItem(itemId));
        } else {
          if (item.kids) {
            item.kids.map(kid => this.store.dispatch(new Item.GetItem(kid)));
          }
        }
        return !!item;
      })
    );
  }

  // Наборы условий после которых происходит рендеринг страницы
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkItemIsLoaded(route.params['id']).pipe(
      map(i => true)
    );
  }
}
