import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromCollection from '../reducers/collections.reducers';
import * as fromReducer from '../index.reducer';
import * as ItemsCollections from '../actions/collection.actions';
import {CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {filter, map, switchMap} from 'rxjs/operators';


@Injectable()
export class JobsCollectionGuard implements CanActivate {
  constructor(private store: Store<fromCollection.State>) {
  }

  waitForLoaded(): Observable<boolean> {
    return this.store
      .select(fromReducer.getJobsLoaded)
      .pipe(
        filter(data => data === true),
        map((data) => data)
      );
  }

  checkTopIsLoaded(): Observable<boolean> {
    return this.store.select(fromReducer.getJobsCollectionIds).pipe(
      map(ids => {
        if (!ids.length) {
          this.store.dispatch(new ItemsCollections.GetJobs());
        }
        return !!ids.length;
      })
    );
  }


  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkTopIsLoaded().pipe(
      switchMap(checked => this.waitForLoaded())
    );
  }
}
