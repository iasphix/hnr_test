import {Injectable} from '@angular/core';
import {Action, Store} from '@ngrx/store';
import * as fromCollection from '../reducers/collections.reducers';
import * as fromReducer from '../index.reducer';
import * as ItemsCollections from '../actions/collection.actions';
import {Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {ItemsService} from '../../items/items.service';
import {concatMap, filter, map, mergeMap, switchMap, take, toArray} from 'rxjs/operators';
import {ItemsCollectionModel} from '../../items/items.model';
import {ITEMS_PER_PAGE} from '../../app.config';


@Injectable()
export class TopCollectionGuard implements CanActivate {
  constructor(private store: Store<fromCollection.State>,
              private itemsService: ItemsService) {
  }

  // Ждём загрузки коллекции Лучших публикаций и только после этого рендерим старницу.
  waitForTopLoaded(): Observable<boolean> {
    return this.store
      .select(fromReducer.getTopLoaded)
      .pipe(
        filter(data => data === true),
        map((data) => data)
      );
  }


  // Проверяем были ли ли уже загружены Лучшие публикации
  checkTopIsLoaded(): Observable<boolean> {
    return this.store.select(fromReducer.getTopCollectionIds).pipe(
      map(ids => {
        if (!ids.length) {
          // если Лучших публикаций нет в стейте, тогда запускаем экшн, который их туда добав
          this.store.dispatch(new ItemsCollections.GetTop());
        }
        return !!ids.length;
      })
    )
  }


  // Наборы условий после которых происходит рендеринг страницы
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkTopIsLoaded().pipe(
      switchMap(checked => this.waitForTopLoaded())
    );
  }
}
