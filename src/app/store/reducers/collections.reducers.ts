import {ItemsCollectionModel} from '../../items/items.model';
import {CollectionAddActionTypes, ItemsCollectionsActions} from '../actions/collection.actions';


export interface State extends ItemsCollectionModel {}

const initialItemsCollectionsState: State = {
    top: [],
    jobs: [],
    loaded: false,
    loading: false,
  };

export function reducer(state = initialItemsCollectionsState, action: ItemsCollectionsActions): State {
  switch (action.type) {
    case CollectionAddActionTypes.LOAD_COLLECTION:
      return {
        ...state,
        loading: true
      };
    case CollectionAddActionTypes.LOAD_COLLECTION_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true
      };
    case CollectionAddActionTypes.LOADED_TOP:
      return {
        ...state,
        top: action.collection,
      };
    case CollectionAddActionTypes.LOADED_JOBS:
      return {
        ...state,
        jobs: action.collection,
      };
    default: {
      return state;
    }
  }
}

export const getTop = (state: State) => state.top;
export const getJobs = (state: State) => state.jobs;

export const getTopLoaded = (state: State) => state.loaded;
export const getJobsLoaded = (state: State) => state.loaded;
