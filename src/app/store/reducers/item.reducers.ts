import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {ItemEntityModel} from '../../items/items.model';
import {ItemActionTypes, ItemsActions} from '../actions/item.actions';


export interface State extends EntityState<ItemEntityModel> {}

export const adapter: EntityAdapter<ItemEntityModel> = createEntityAdapter<ItemEntityModel>({
  selectId: (item: ItemEntityModel) => item.id,
  sortComparer: false,
});

const initialState: State = adapter.getInitialState();

export function reducer(
  state: State = initialState,
  action: ItemsActions,
): State {
  switch (action.type) {
    case ItemActionTypes.ITEM_ADD:
      return adapter.addOne(action.item, state);
    default:
      return state;
  }
}
