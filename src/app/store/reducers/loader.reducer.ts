import {LoaderActions, LoaderActionTypes} from '../actions/loader.actions';


export interface State {
  loaded: boolean;
  loading: boolean;
}

const initialItemsCollectionsState: State = {
  loaded: false,
  loading: false,
};

export function reducer(state = initialItemsCollectionsState, action: LoaderActions): State {
  switch (action.type) {
    case LoaderActionTypes.LOADER_SHOW:
      return {
        ...state,
        loading: true
      };
    case LoaderActionTypes.LOADER_HIDE:
      return {
        ...state,
        loading: false,
        loaded: true
      };
    default: {
      return state;
    }
  }
}

export const getLoading = (state: State) => state.loading;
