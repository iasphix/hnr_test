import * as fromItemsCollections from './reducers/collections.reducers';
import * as fromItem from './reducers/item.reducers';
import * as fromLoader from './reducers/loader.reducer';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

export interface AppState{
  router: fromRouter.RouterReducerState;
  'collections': fromItemsCollections.State;
  'items': fromItem.State;
  'loader': fromLoader.State;
}

export const reducers = {
  router: fromRouter.routerReducer,
  collections: fromItemsCollections.reducer,
  items: fromItem.reducer,
  loader: fromLoader.reducer
};

export const getCollectionsState = createFeatureSelector<fromItemsCollections.State>('collections');
export const getItemsState = createFeatureSelector<fromItem.State>('items');
export const getLoaderState = createFeatureSelector<fromLoader.State>('loader');


export const getTopCollectionIds = createSelector(getCollectionsState, fromItemsCollections.getTop);
export const getJobsCollectionIds = createSelector(getCollectionsState, fromItemsCollections.getJobs);

export const getItemsEntitiesState = createSelector(
  getItemsState,
    state => state
);


export const {
  selectIds: getItemIds,
  selectEntities: getItemEntities,
  selectAll: getAllItems,
  selectTotal: getTotalItems,
} = fromItem.adapter.getSelectors(getItemsEntitiesState);


export const getSingleItem = (id) => createSelector(
  getItemEntities,
  (entities) => {
    return entities[id];
  }
);

export const getTopItemsCollection = (startIndex, limit) => createSelector(
  getItemEntities,
  getTopCollectionIds,
  (entities, ids) => {
      return ids.slice(startIndex, startIndex + limit).map(id => entities[id]);
  }
);

export const getTopLoaded = createSelector(
  getCollectionsState,
  fromItemsCollections.getTopLoaded
);

export const getJobsLoaded = createSelector(
  getCollectionsState,
  fromItemsCollections.getJobsLoaded
)

export const getItemKids = (kidsIds) => createSelector(
  getItemEntities,
  (entities) => kidsIds.map(id => entities[id])
)

export const getLoaderStatus = createSelector(
  getLoaderState,
  fromLoader.getLoading
)


export const getJobsItemsCollection = (startIndex, limit) => createSelector(
  getItemEntities,
  getJobsCollectionIds,
  (entities, ids) => {
    return ids.slice(startIndex, startIndex + limit).map(id => entities[id]);
  }
);
