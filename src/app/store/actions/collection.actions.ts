import {Action} from '@ngrx/store';

export enum CollectionAddActionTypes {
  LOAD_COLLECTION = 'Загрузка коллекции',
  LOAD_COLLECTION_SUCCESS = 'Коллекция успешно загружена',
  ADD_COLLECTION_TOP = 'Добавить коллекцию лучших публикаций',
  ADD_COLLECTION_JOBS = 'Добавить коллекцию вакансий/работы',
  LOADED_TOP = 'Коллекция лучших публикаций загружена',
  LOADED_JOBS = 'Коллекция работ загружена'
}

export class GetTop implements Action {
  readonly type = CollectionAddActionTypes.ADD_COLLECTION_TOP;
}


export class LoadedTop implements Action {
  readonly type = CollectionAddActionTypes.LOADED_TOP;

  constructor(public collection: number[]) {
  }
}

export class GetJobs implements Action {
  readonly type = CollectionAddActionTypes.ADD_COLLECTION_JOBS;
}

export class LoadedJobs implements Action {
  readonly type = CollectionAddActionTypes.LOADED_JOBS;

  constructor(public collection: number[]) {
  }
}

export class LoadCollection implements Action {
  readonly type = CollectionAddActionTypes.LOAD_COLLECTION;
}

export class LoadCollectionSuccess implements Action {
  readonly type = CollectionAddActionTypes.LOAD_COLLECTION_SUCCESS;
}

export type ItemsCollectionsActions = LoadCollection
  | LoadCollectionSuccess
  | GetJobs
  | GetTop
  | LoadedTop
  | LoadedJobs;


export type ItemsCollectionsLoaded = LoadedTop
  | LoadedJobs;
