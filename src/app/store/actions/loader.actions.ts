import {Action} from '@ngrx/store';

export enum LoaderActionTypes {
  LOADER_SHOW = 'Show loader',
  LOADER_HIDE = 'Hide loader',
}

export class ShowLoader implements Action {
  readonly type = LoaderActionTypes.LOADER_SHOW;
}

export class HideLoader implements Action {
  readonly type = LoaderActionTypes.LOADER_HIDE;
}

export type LoaderActions = ShowLoader
  | HideLoader;

