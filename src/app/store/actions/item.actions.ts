import {Action} from '@ngrx/store';
import {ItemEntityModel, ItemsModel} from '../../items/items.model';

export enum ItemActionTypes {
  ITEM_GET = '[Items] Get one Item',
  ITEM_ADD = '[Items] Add one Item',
}


export class AddItem implements Action {
  readonly type = ItemActionTypes.ITEM_ADD;

  constructor(public item: ItemEntityModel) {
  }
}

export class GetItem implements Action {
  readonly type = ItemActionTypes.ITEM_GET;

  constructor(public itemId: number) {
  }
}


export type ItemsActions = AddItem
  | GetItem;
