import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import * as ItemsCollections from '../actions/collection.actions';
import * as Item from '../actions/item.actions';
import * as Loader from '../actions/loader.actions';
import {map} from 'rxjs/operators';

@Injectable()
export class LoaderEffects {
  constructor(private actions$: Actions) {
  }
////////////////////////////////////////////////////////////////////////////////////////////////////
// Эффекты отображения загрузчика
////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  showLoader$: Observable<Action> = this.actions$
    .ofType(
      ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_TOP,
      ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_JOBS,
      Item.ItemActionTypes.ITEM_GET
    )
    .pipe(
      map(() => new Loader.ShowLoader())
    );

  @Effect()
  hideLoader$: Observable<Action> = this.actions$
    .ofType(
      ItemsCollections.CollectionAddActionTypes.LOADED_TOP,
      ItemsCollections.CollectionAddActionTypes.LOADED_JOBS,
      Item.ItemActionTypes.ITEM_ADD
    )
    .pipe(
      map(() => new Loader.HideLoader())
    );
}
