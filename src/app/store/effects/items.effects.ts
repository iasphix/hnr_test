import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';
import * as ItemsCollections from '../actions/collection.actions';
import * as Item from '../actions/item.actions';
import {map, mergeMap, switchMap, take} from 'rxjs/operators';
import {ItemsService} from '../../items/items.service';
import {ITEMS_PER_PAGE} from '../../app.config';
import {Database} from '@ngrx/db';
import {defer} from 'rxjs/observable/defer';
import 'rxjs/add/observable/from';
import {ItemEntityModel} from '../../items/items.model';
import * as dbSchema from '../../app-db';


@Injectable()
export class ItemsEffects {

  constructor(private actions$: Actions,
              private db: Database,
              private itemsService: ItemsService) {
  }

  // Открытие соединения с IndexedDB
  @Effect({dispatch: false})
  openDB$: Observable<any> = defer(() => {
    return this.db.open(dbSchema.IndexedDBSchema.name);
  });

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Эффекты загрузки коллекции Лучших публикаций
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  loadTop$: Observable<Action> = this.actions$
    .ofType(ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_TOP)
    .pipe(
      switchMap((data) => this.itemsService.getTop()), // запрос на загрузку из API
      map((response: number[]) => new ItemsCollections.LoadedTop(response) // Возвращаем результат из предыдущей операции в Экшн
      )
    );

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Эффекты загрузки коллекции вакансий
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  loadJobs$: Observable<Action> = this.actions$
    .ofType(ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_JOBS)
    .pipe(
      switchMap((data) => {
        return this.itemsService.getJobs()
          .pipe(
            map((result: number[]) => new ItemsCollections.LoadedJobs(result)));
      })
    );

  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Эффекты состояния загрузки коллекций публикаций
  // (для отобрражения ленты только после прогрузки коллекции)
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  loadCollection$: Observable<Action> = this.actions$
    .ofType(
      ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_TOP,
      ItemsCollections.CollectionAddActionTypes.ADD_COLLECTION_JOBS
    )
    .pipe(
      map(() => new ItemsCollections.LoadCollection())
    );

  @Effect()
  loadCollectionSuccess$: Observable<Action> = this.actions$
    .ofType(
      ItemsCollections.CollectionAddActionTypes.LOADED_TOP,
      ItemsCollections.CollectionAddActionTypes.LOADED_JOBS
    )
    .pipe(
      map(() => new ItemsCollections.LoadCollectionSuccess())
    );


  ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Эффекты эффекты загрузки отдельных публикаций коллекций
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  loadItems$: Observable<Action> = this.actions$
    .ofType(
      ItemsCollections.CollectionAddActionTypes.LOADED_TOP,
      ItemsCollections.CollectionAddActionTypes.LOADED_JOBS
    )
    .pipe(
      switchMap((data: ItemsCollections.ItemsCollectionsLoaded) => {
        // Преобразуем коллекцию с айдишниками в Observable,
        // берём N количество первых ids, для первичной загрузки данных
        return Observable.from(data.collection).pipe(
          take(ITEMS_PER_PAGE)
        );
      }),
      map((itemId: number) => {
        return new Item.GetItem(itemId);
      })
    );

////////////////////////////////////////////////////////////////////////////////////////////////////
// Эффекты загрузки отдельной публикации
////////////////////////////////////////////////////////////////////////////////////////////////////
  @Effect()
  loadItem$: Observable<Action> = this.actions$
    .ofType(Item.ItemActionTypes.ITEM_GET)
    .pipe(
      switchMap((item: any) => Observable.from([item.itemId]).pipe(take(1))),
      mergeMap(itemId => {
        return this.db.get(dbSchema.IndexedDBStoreNames.ITEMS, itemId)
          .pipe(
            map(i => [!!i, itemId])
          );
      }),
      mergeMap(([dbItem, itemId]: [boolean, number]) => {
        // если в БД уже есть публикация с Id = itemId  - передаём дальше её.
        if (dbItem) {
          return this.db.get(dbSchema.IndexedDBStoreNames.ITEMS, itemId).pipe(map(i => i));
        } else {
          // либо загружаем из API
          return this.itemsService.getItem(itemId)
            .pipe(
              // Сохраняем загруженную публикацию в базе
              switchMap(response => this.db.insert(dbSchema.IndexedDBStoreNames.ITEMS, [response])),
              // Передаём данные публикации дальше
              map((response) => response)
            );
        }
      }),
      map((data: ItemEntityModel) => {
        return new Item.AddItem(data);
      })
    );
}
