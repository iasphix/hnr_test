// Api
export const API_URL = 'https://hacker-news.firebaseio.com/v0/';

export const API_TOP_URL = 'topstories.json';
export const API_NEW_URL = 'newstories.json';
export const API_BEST_URL = 'beststories.json';

export const API_JOBS_URL = 'jobstories.json';
export const API_SHOWS_URL = 'showstories.json';
export const API_ASK_URL = 'askstories.json';

export const API_ITEM_URL = 'item';

export const API_USER_URL = 'user';

export const API_UPDATE_URL = 'updates.json';
export const API_LAST_ITEM_URL = 'maxitem.json';

// Items Settings

export enum ITEM_TYPE {
  STORY = 'story',
  COMMENT = 'comment',
  ASK = 'ask',
  JOB = 'job',
  POLL_OPT = 'pollopt',
  POLL = 'poll'
}

export const ITEMS_PER_PAGE = 25;
