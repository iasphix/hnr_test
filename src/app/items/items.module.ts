import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ItemsRoutingModule} from './items-routing.module';
import {ItemsComponent} from './items.component';
import {TopCollectionGuard} from '../store/guards/top.guard';
import {ActivatedRouteSnapshot} from '@angular/router';
import { CommentComponent } from './comment/comment.component';
import { SingleItemComponent } from './single-item/single-item.component';
import {SafeHtmlPipe} from '../shared/pipes/safe-html.pipe';

@NgModule({
  imports: [
    CommonModule,
    ItemsRoutingModule
  ],
  declarations: [
    ItemsComponent,
    CommentComponent,
    SingleItemComponent,
    SafeHtmlPipe
  ]
})
export class ItemsModule { }
