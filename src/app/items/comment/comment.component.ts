import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ItemEntityModel} from '../items.model';
import {Store} from '@ngrx/store';
import {AppState, getSingleItem} from '../../store/index.reducer';
import {GetItem} from '../../store/actions/item.actions';

@Component({
  selector: 'app-item-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CommentComponent implements OnInit {
  @Input() commentId: number;
  comment$: Observable<ItemEntityModel>;

  constructor(
    private store: Store<AppState>,
  ) {
  }

  ngOnInit() {
    this.store.dispatch(new GetItem(this.commentId));
    this.comment$ = this.store.select(getSingleItem(this.commentId));
  }

}
