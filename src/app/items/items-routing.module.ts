import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ItemsComponent} from './items.component';
import {ItemGuard} from '../store/guards/item.guard';

const routes: Routes = [{
  path: '',
  component: ItemsComponent,
  canActivate: [ItemGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ItemGuard]
})
export class ItemsRoutingModule { }
