import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-item-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemPostComponent implements OnInit {
  @Input() item;

  constructor() {
  }

  ngOnInit() {
  }

  outerLinkClick(event) {
    return event.stopPropagation();
  }

}
