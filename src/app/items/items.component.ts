import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {ItemEntityModel, ItemsRouteParams} from './items.model';
import {AppState} from '../store/index.reducer';
import {ActivatedRoute} from '@angular/router';
import * as mainReducers from '../store/index.reducer';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemsComponent implements OnInit {
  item$: Observable<ItemEntityModel>;

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute) {

    this.route.params.subscribe((param: ItemsRouteParams) => {
      if (param.id) {
        this.item$ = this.store.select(mainReducers.getSingleItem(param.id));
      }
    });
  }

  ngOnInit() {
  }

}
