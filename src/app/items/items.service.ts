import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as mainReducers from '../store/index.reducer';
import * as Item from '../store/actions/item.actions';
import {APIUtils} from '../shared/utils';
import {API_ITEM_URL, API_JOBS_URL, API_TOP_URL, ITEMS_PER_PAGE} from '../app.config';
import {Observable} from 'rxjs/Observable';
import {ItemEntityModel} from './items.model';
import {Store} from '@ngrx/store';
import {AppState} from '../store/index.reducer';
import {map, switchMap} from 'rxjs/operators';
import {GetItem} from '../store/actions/item.actions';

@Injectable()
export class ItemsService {

  constructor(private $http: HttpClient,
              private store: Store<AppState>,
              private apiUtils: APIUtils) {
  }

  getItem(itemId: number) {
    return this.$http.get(this.apiUtils.generateFullAPIURL(API_ITEM_URL, `/${itemId}.json`));
  }

  getTop() {
    return this.$http.get(this.apiUtils.generateFullAPIURL(API_TOP_URL));
  }

  getJobs() {
    return this.$http.get(this.apiUtils.generateFullAPIURL(API_JOBS_URL));
  }

}
