import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {ItemEntityModel} from '../items.model';

@Component({
  selector: 'app-single-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SingleItemComponent implements OnInit {
  @Input() item: ItemEntityModel
  constructor() { }

  ngOnInit() {
  }

}
