import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {environment} from '../environments/environment';
import {IndexedDBSchema} from './app-db';

import {ServiceWorkerModule} from '@angular/service-worker';

import {AppRoutingModule} from './app-routing.module';
import {DBModule} from '@ngrx/db';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {APIUtils} from './shared/utils';
import {ItemsEffects} from './store/effects/items.effects';
import {LoaderEffects} from './store/effects/loader.effects';
import {HttpClientModule} from '@angular/common/http';
import {ItemsService} from './items/items.service';
import {HeaderComponent} from './shared/layout/header/header.component';
import {reducers} from './store/index.reducer';
import {LoaderComponent} from './shared/loader/loader.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatProgressBarModule} from '@angular/material';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatProgressBarModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([LoaderEffects, ItemsEffects]),
    //!environment.production ? StoreDevtoolsModule.instrument({maxAge: 40}) : [],
    StoreRouterConnectingModule,
    DBModule.provideDB(IndexedDBSchema),
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : []
  ],
  providers: [
    ItemsService,
    APIUtils
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
