import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TopComponent} from './top.component';
import {TopCollectionGuard} from '../store/guards/top.guard';

const routes: Routes = [{
  path: '',
  component: TopComponent,
  canActivate: [TopCollectionGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [TopCollectionGuard]
})
export class TopRoutingModule { }
