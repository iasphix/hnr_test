import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as ItemsCollections from '../store/actions/collection.actions';
import {AppState} from '../store/index.reducer';
import * as mainReducers from '../store/index.reducer';
import {Observable} from 'rxjs/Observable';
import {ItemEntityModel} from '../items/items.model';
import {ITEMS_PER_PAGE} from '../app.config';
import {ChangeEvent, VirtualScrollComponent} from 'angular2-virtual-scroll';
import {GetItem} from '../store/actions/item.actions';
import {ItemsService} from '../items/items.service';


@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopComponent implements OnInit {
  @ViewChild(VirtualScrollComponent)
  private virtualScroll: VirtualScrollComponent;
  scrollItems: Event;
  items: ItemEntityModel[];
  topCollectionIds: number[];

  constructor(
    private store: Store<AppState>,
    private itemsService: ItemsService
  ) {

    this.onResize();

    this.store.select(mainReducers.getTopItemsCollection(0, ITEMS_PER_PAGE)).subscribe(i => this.items = i);
    this.store.select(mainReducers.getTopCollectionIds).subscribe(ids => this.topCollectionIds = ids);
  }


  loadMore(event: ChangeEvent) {
    const itemsLength = this.items.length;
    if (event.end !== itemsLength || itemsLength === this.topCollectionIds.length) {
      return;
    }
    this.topCollectionIds.slice(itemsLength, itemsLength + ITEMS_PER_PAGE)
      .map(id => this.store.dispatch(new GetItem(id)));
    this.store.select(mainReducers.getTopItemsCollection(itemsLength, ITEMS_PER_PAGE))
      .subscribe( (i) => this.items.push(i[0]));
  }

  onResize() {
    window.onresize = (e) => {
      this.virtualScroll.refresh();
    };
  }

  ngOnInit() {

  }

}
