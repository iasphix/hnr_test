import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopRoutingModule } from './top-routing.module';
import { TopComponent } from './top.component';
import {VirtualScrollModule} from 'angular2-virtual-scroll';
import {ItemPostComponent} from '../items/post/post.component';
import {CollectionsModule} from '../collections/collections.module';

@NgModule({
  imports: [
    CommonModule,
    CollectionsModule,
    TopRoutingModule,
  ],
  declarations: [TopComponent]
})
export class TopModule { }
