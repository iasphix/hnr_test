import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import * as ItemsCollections from '../store/actions/collection.actions';
import {AppState} from '../store/index.reducer';
import {ChangeEvent, VirtualScrollComponent} from 'angular2-virtual-scroll';
import {ItemEntityModel} from '../items/items.model';
import * as mainReducers from '../store/index.reducer';
import {ITEMS_PER_PAGE} from '../app.config';
import {GetItem} from '../store/actions/item.actions';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobsComponent implements OnInit {
  @ViewChild(VirtualScrollComponent)
  private virtualScroll: VirtualScrollComponent;
  scrollItems: Event;
  items: ItemEntityModel[];
  jobsCollectionIds: number[];

  constructor(private store: Store<AppState>) {
    this.onResize();
    this.store.select(mainReducers.getJobsItemsCollection(0, ITEMS_PER_PAGE)).subscribe(i => this.items = i);
    this.store.select(mainReducers.getJobsCollectionIds).subscribe(ids => this.jobsCollectionIds = ids);
  }


  loadMore(event: ChangeEvent) {
    const itemsLength = this.items.length;
    if (event.end !== itemsLength || itemsLength === this.jobsCollectionIds.length) {
      return;
    }
    this.jobsCollectionIds.slice(itemsLength, itemsLength + ITEMS_PER_PAGE)
      .map(id => this.store.dispatch(new GetItem(id)));
    this.store.select(mainReducers.getJobsItemsCollection(itemsLength, ITEMS_PER_PAGE))
      .subscribe((i) => this.items.push(i[0]));
  }

  ngOnInit() {
  }


  onResize() {
    window.onresize = (e) => {
      this.virtualScroll.refresh();
    };
  }

}
