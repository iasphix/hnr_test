import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {JobsComponent} from './jobs.component';
import {JobsCollectionGuard} from '../store/guards/jobs.guard';

const routes: Routes = [{
  path: '',
  component: JobsComponent,
  canActivate: [JobsCollectionGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [JobsCollectionGuard]
})
export class JobsRoutingModule { }
