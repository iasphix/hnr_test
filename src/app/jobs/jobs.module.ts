import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsComponent } from './jobs.component';
import {CollectionsModule} from '../collections/collections.module';

@NgModule({
  imports: [
    CommonModule,
    CollectionsModule,
    JobsRoutingModule,
  ],
  declarations: [JobsComponent]
})
export class JobsModule { }
