import { DBSchema } from '@ngrx/db';

/**
 * ngrx/db uses a simple schema config object to initialize stores in IndexedDB.
 */
export const IndexedDBSchema: DBSchema = {
  version: 1,
  name: 'hackernews_app',
  stores: {
    items: {
      autoIncrement: true,
      primaryKey: 'id',
    },
    users: {
      autoIncrement: true,
      primaryKey: 'id'
    }
  },
};

export enum IndexedDBStoreNames {
  ITEMS = 'items'
}
